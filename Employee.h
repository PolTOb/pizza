#ifndef EMPLOYEE_H_INCLUDED
#define EMPLOYEE_H_INCLUDED
#include "Restaurant.h"
class Employee{
private:
    string name;
	double salary;
	string security_number;
	string department_phone;
	int hours;
public:
    Employee(string,double,string,string);
    string getName();
    double getSalary();
    string getSecurity_number();
    string getDepartment_phone();
    void Show();
    int Work();
};
#endif // EMPLOYEE_H_INCLUDED
