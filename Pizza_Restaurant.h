#ifndef PIZZA_RESTAURANT_H_INCLUDED
#define PIZZA_RESTAURANT_H_INCLUDED
#include "Restaurant.h"
#include <iostream>
using namespace std;
class Pizza_Restaurant:public Restaurant{
private:
    string name;
    string address;
public:
    Pizza_Restaurant(string,string);
    string getName();
    string getAddress();
};


#endif // PIZZA_RESTAURANT_H_INCLUDED
